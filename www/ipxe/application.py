import glob
import os.path
import re
from urllib.parse import parse_qs

import psycopg2
import yaml
from psycopg2.extras import RealDictCursor

ipxedir = "/etc/ipxeconf/"
dbconn = "/etc/database-connect.yml"
ipxeconf = ipxedir + "ipxe_config.yml"

# mac = '52:54:00:12:34:56' #virtual machine, doesn't
# mac = '18:03:73:40:d8:73' # stockpc3, works


def getconfig(conffilename):
    with open(conffilename, "r") as conffile:
        config = yaml.load(conffile, Loader=yaml.SafeLoader)
    return config


def opendb():
    psql = getconfig(dbconn)
    conn = psycopg2.connect(
        dbname=psql["dbname"],
        user=psql["user"],
        host=psql["host"],
        password=psql["passwd"],
    )
    return conn


def getMac(parameters):
    if "mac" in parameters:
        mac = re.search(
            r"^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$",
            parameters["mac"][0],
        )
        if mac is not None:
            return mac.group()
    else:
        return None


def getmachinedetails(db, mac):
    if mac:
        query = (
            "SELECT * "
            "FROM apps.autobuild "
            "WHERE %s::macaddr IN "
            "  (wired_mac_1, wired_mac_2, wired_mac_3, wired_mac_4, wireless_mac)"
        )
        cur = db.cursor(cursor_factory=RealDictCursor)
        cur.execute(query, [mac])
        result = cur.fetchall()

        if not len(result) > 0:
            return False
        else:
            # has keys 'reinstall', 'os', 'arch', 'netboot' and 'hostname'
            # (amongst others)
            return result[0]
    else:
        return False


def getfullos(machinedetails):
    return (
        machinedetails["os"].lower().replace(" ", "_")
        + "_"
        + machinedetails["arch"].lower()
    )


def getbooturllines(conf):
    # Generate the list of boot urls from the boot_urls dict in the config
    urllist = [" ".join(["set", i, j]) for i, j in conf["boot_urls"].items()]
    return "\n".join(urllist) + "\n"


def reinstall(conf, machinedetails):
    pagebuffer = ""
    if machinedetails["os"].lower().startswith("windows"):
        try:
            pagebuffer += conf["autoinstallers"][getfullos(machinedetails)] + "\n\n"
        except KeyError:
            pagebuffer += conf["autoinstallers"]["windows"] + "\n\n"
    else:
        try:
            pagebuffer += conf["autoinstallers"][getfullos(machinedetails)] + "\n\n"
        except KeyError:
            pagebuffer += (
                "echo Automatic installation, "
                + getfullos(machinedetails)
                + " config not found, dropping to shell\n"
            )
            pagebuffer += "echo Type exit to continue to the iPXE menu\n"
            pagebuffer += "shell\n\n"
    return pagebuffer


def genMenu():
    pagebuffer = ""
    itembuff = "\n"
    for root, dirs, files in os.walk(ipxedir + "menu"):
        # menu title and head
        dirconf = getconfig(os.path.join(root, "00_head.yml"))
        mname = root.split("/")[-1]
        pagebuffer += ":menu-" + mname + "\n"
        pagebuffer += "menu " + dirconf["name"] + "\n"

        if "head" in dirconf:
            pagebuffer += dirconf["head"] + "\n"

        # menu items
        if mname != "menu":
            pagebuffer += "item --key b menu-" + root.split("/")[-2] + " (b) Go back\n"

        # sub menus
        for d in sorted(dirs):
            dconf = getconfig(os.path.join(root, d, "00_head.yml"))
            pagebuffer += "item "

            if "key" in dconf:
                pagebuffer += (
                    "--key "
                    + str(dconf["key"])
                    + " menu-"
                    + d
                    + " ("
                    + str(dconf["key"])
                    + ") "
                )
            else:
                pagebuffer += "menu-" + d + " "
            pagebuffer += dconf["name"] + " -->\n"

        # menu items
        for f in sorted(glob.glob(os.path.join(root, "*.yml*"))):
            fname = f.split("/")[-1].split(".")[0]
            if fname == "00_head" or fname == "99_footer":
                continue
            fconf = getconfig(f)
            pagebuffer += "item "
            if "key" in fconf:
                pagebuffer += (
                    "--key "
                    + str(fconf["key"])
                    + " "
                    + fname
                    + " ("
                    + str(fconf["key"])
                    + ") "
                )
            else:
                pagebuffer += fname + " "
            pagebuffer += fconf["name"] + "\n"
            itembuff += ":" + fname + "\n" + fconf["run"] + "\n"

        try:
            footer_config = getconfig(os.path.join(root, "99_footer.yml"))
            if "footer" in footer_config:
                pagebuffer += footer_config["footer"]

        except FileNotFoundError:
            pass

        # end of the menu
        pagebuffer += "\nisset ${menu-timeout} || set menu-timeout 0"
        pagebuffer += "\nchoose --timeout ${menu-timeout}"
        if "default" in dirconf:
            pagebuffer += (
                " --default " + dirconf["default"] + " selected || goto cancel\n"
            )
        else:
            pagebuffer += " selected || goto cancel\n"
        pagebuffer += "set menu-timeout 0\n"
        pagebuffer += "goto ${selected}\n\n"

    pagebuffer += itembuff
    return pagebuffer


def application(environ, start_response):
    conf = getconfig(ipxeconf)
    parameters = parse_qs(environ.get("QUERY_STRING", ""))
    if "mac" in parameters:
        db = opendb()

        # Find the mac address of the machin
        mac = getMac(parameters)

        machinedetails = getmachinedetails(db, mac)

        pagebuffer = conf["confhead"]
        pagebuffer += getbooturllines(conf) + "\n"

        # if the mac address was found, make a note of the hostname
        if machinedetails and machinedetails["hostname"]:
            pagebuffer += "set dbhostname {hostname}\n".format(
                hostname=machinedetails["hostname"]
            )
            pagebuffer += "set shorthostname {shorthostname}\n".format(
                shorthostname=machinedetails["hostname"].split(".")[0]
            )
        else:
            # otherwise provide a sensible default
            pagebuffer += "set dbhostname unknown-hostname\n"
            pagebuffer += "set shorthostname unknown-hostname\n"
        pagebuffer += "\n"

        # if the mac address wasn't found
        if not machinedetails:
            pagebuffer += (
                """
            echo
            echo MAC address not found in database
            echo Starting self-registration kiosk mode
            echo"""
                + "\n"
                + conf["nomacaddress"]
            )

        # do we have netboot set?
        elif machinedetails["netboot"]:
            pagebuffer += "goto %s\n" % machinedetails["netboot"]

        # Do we reinstall?
        elif machinedetails["reinstall"]:
            pagebuffer += reinstall(conf, machinedetails)
        # Always have Normal Boot Menu
        pagebuffer += genMenu()

        pagebuffer += conf["conftail"]

        start_response("200 OK", [("Content-Type", "text/plain")])
        return [pagebuffer.encode()]
    else:
        start_response("400 Bad Request", [("Content-Type", "text/plain")])
        return ["mac not supplied".encode()]


if __name__ == "__main__":
    from wsgiref.simple_server import make_server

    srv = make_server("localhost", 8080, application)
    srv.serve_forever()
