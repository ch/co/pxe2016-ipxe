#!/usr/bin/env python
import re
from urllib.parse import parse_qs

import psycopg2
import yaml
from psycopg2.extras import RealDictCursor

conffilename = "/etc/database-connect.yml"


def getConfig(conffilename):
    with open(conffilename, "r") as conffile:
        config = yaml.load(conffile, Loader=yaml.SafeLoader)
    return config


def dbConnect(config):
    conn = psycopg2.connect(
        dbname=config["dbname"],
        user=config["user"],
        host=config["host"],
        password=config["passwd"],
    )
    conn.autocommit = True
    return conn


def isRegistered(mac, db):
    query = (
        "SELECT count(*) "
        "FROM system_image "
        "WHERE %s::macaddr in "
        "  (wired_mac_1, wired_mac_2, wired_mac_3, wired_mac_4, wireless_mac)"
    )
    cur = db.cursor(cursor_factory=RealDictCursor)
    cur.execute(query, [mac])
    result = cur.fetchall()

    if result[0]["count"] == 0:
        return False
    else:
        return True


def getToken(mac, db):
    query = "SELECT ensure_machine_account(%s::macaddr)"
    cur = db.cursor(cursor_factory=RealDictCursor)
    cur.execute(query, [mac])
    result = cur.fetchall()
    return result[0]["ensure_machine_account"]


def getMac(parameters):
    if "mac" in parameters:
        mac = re.search(
            r"^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$",
            parameters["mac"][0],
        )
        if mac is not None:
            return mac.group()
    else:
        return None


def application(environ, start_response):
    config = getConfig(conffilename)
    parameters = parse_qs(environ.get("QUERY_STRING", ""))
    if "mac" in parameters:
        # Find the mac address of the machine
        mac = getMac(parameters)
        if mac is not None:
            db = dbConnect(config)
            # Is the mac address in the database?
            if isRegistered(mac, db):
                start_response("200 OK", [("Content-Type", "text/plain")])
                resp = "%s" % str(getToken(mac, db))
                return [resp.encode()]
            else:
                start_response("400 Bad Request", [("Content-Type", "text/plain")])
                resp = 'Mac "%s" is not in database' % mac
                return [resp.encode()]
        else:
            start_response("422 Unprocessable Entity", [("Content-Type", "text/plain")])
            resp = 'Could not turn "%s" into mac address"' % mac
            return [resp.encode()]
    else:
        start_response("400 Bad Request", [("Content-Type", "text/plain")])
        resp = "mac not supplied"
        return [resp.encode()]


if __name__ == "__main__":
    from wsgiref.simple_server import make_server

    srv = make_server("localhost", 8080, application)
    srv.serve_forever()
