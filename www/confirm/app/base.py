#!/usr/bin/env python

import os
import sqlite3

import psycopg2
from app import conf


class postgresDB(object):
    def __init__(self):
        self._db_conn()

    def _db_conn(self):
        self.conn = psycopg2.connect(
            dbname=conf.dbname, user=conf.user, password=conf.passwd, host=conf.host
        )

    def execute_query(self, query, vals=None):
        cursor = self.conn.cursor()
        try:
            cursor.execute(query, vals)
            self.conn.commit()
            return cursor.fetchall()
        except psycopg2.ProgrammingError:
            self.conn.rollback()
        return False


class sqlliteDB(object):
    def __init__(self):
        self._dbfile = os.path.join(conf.sqlite_dblocation, conf.sqlite_dbname)
        self._init_db()

    def _init_db(self):
        if not os.path.exists(self._dbfile):
            for query in conf.sqlite_newdb:
                self.execute_query(query=query)

    @property
    def _db_conn(self):
        return sqlite3.connect(self._dbfile)

    def execute_query(self, query, vals={}):
        """Attempt to execute a query

        Args:
            query (sqlite query)
        Returns:
            query result if successful. False if not.
        """
        result = False
        with self._db_conn as conn:
            cursor = conn.cursor()
            if isinstance(query, list):
                result = []
                for q in query:
                    result.append(cursor.execute(q, vals))
            else:
                result = cursor.execute(query, vals)
        return result
