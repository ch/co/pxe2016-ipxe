from __future__ import absolute_import

import config
from flask import Flask

conf = config.config()

app = Flask(__name__)

# Set the config from conf.flask
for item, value in conf.flask.items():
    app.config[item] = value

from app import views  # noqa F401, E402 needed for Flask to work
