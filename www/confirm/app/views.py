import re

import flask
from app import app, base, conf
from flask import flash, render_template, request, send_from_directory
from six.moves.urllib import parse

sqlliteDB = base.sqlliteDB()
psqlDB = base.postgresDB()


@app.route("/release", methods=("GET", "POST"))
def release():
    if request.method == "POST" and request.form["release"]:
        if toggle_release_machine(request.form["release"]):
            flash('Toggled release on "%s"' % request.form["release"], "success")
        else:
            flash('Unable to toggle "%s"' % request.form["release"], "alert")
        return flask.redirect(flask.url_for("release"))

    machines = get_machines()
    return render_template("release.j2", machines=machines)


@app.route("/pl/<path:path>")
def send_pl(path):
    # This is for testing. /var/www/pl probably exists on the server.
    # If it does - make sure you configure apache properly
    return send_from_directory("project-light-minimal/pl", path)


@app.route("/loop")
def make_loop():
    if "mac" in request.args and check_mac(parse.unquote(request.args["mac"])):
        # Do a query, see if the db says true
        mac = parse.unquote(request.args["mac"])
        add_to_sqlitedb(mac)
        return conf.ipxe_loop
    else:
        return conf.ipxe_loop


@app.route("/confirm")
def confirm():
    if "mac" in request.args and check_mac(parse.unquote(request.args["mac"])):
        # Do a query, see if the db says true
        mac = parse.unquote(request.args["mac"])
        add_to_sqlitedb(mac)
        release = check_release_machine(mac)
        return conf.ipxe_confirm.format(release)
    else:
        # Make it return a default of false if something is wrong.
        return conf.ipxe_confirm.format("False")


def add_to_sqlitedb(mac):
    hostname = get_hostname(mac)
    dct = {"mac": mac, "hostname": hostname}
    if hostname:
        query = sqlliteDB.execute_query(conf.sqllite_update_sql, dct)
        return query


def toggle_release_machine(mac):
    if check_mac(mac):
        dct = {"mac": mac}
        query = conf.sqllite_release_sql
        query = sqlliteDB.execute_query(query, dct)
        return True
    else:
        return False


def check_release_machine(mac):
    release = 0
    dct = {"mac": mac}
    query = conf.sqllite_check_release
    result = sqlliteDB.execute_query(query, dct).fetchone()
    if result:
        release = result[0]
    # we get 0 for false, 1 for true
    return release == 1


def get_machines():
    query = sqlliteDB.execute_query(conf.sqllite_select_machines)
    result = query.fetchall()
    fields = conf.sqllite_machines_fields
    machines = [dict(zip(fields, res)) for res in result]
    return machines


def get_hostname(mac):
    query = conf.psql_get_hostname
    result = psqlDB.execute_query(query, {"mac": mac})
    if result:
        return result[0][0]


def check_mac(mac):
    regex = r"^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$"
    return re.search(regex, mac) is not None
