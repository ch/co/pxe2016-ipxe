#!/usr/bin/env python

import os

import yaml

# Where this lives
basedir = os.path.abspath(os.path.dirname(__file__))

# Config files
config_filenames = ["confirm-config.yml", "database-connect.yml", "confirm-flask.yml"]

# Where to look for the config files
search_paths = [
    os.path.join(os.sep, "etc", "ipxeconf"),
    os.path.join(os.sep, "etc"),
    os.path.join(basedir),
]


class config:
    def __init__(self):
        for conffile in config_filenames:
            candidates = [os.path.join(p, conffile) for p in search_paths]
            configfile = self._get_first_config_found(candidates)
            self._setconfig(self._getconfig(configfile))

    def _get_first_config_found(self, search_paths):
        for fname in search_paths:
            if os.path.exists(fname):
                return fname
        return False

    def _getconfig(self, conffilename):
        if conffilename:
            with open(conffilename, "r") as conffile:
                config = yaml.load(conffile, Loader=yaml.SafeLoader)
            return config

    def _setconfig(self, config):
        # If we don't find the config file, we get none
        if config:
            for item in config.keys():
                setattr(self, item, config[item])
